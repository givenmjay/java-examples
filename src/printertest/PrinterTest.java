/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package printertest;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

/**
 *
 * @author given
 */
public class PrinterTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PrinterJob job = PrinterJob.getPrinterJob();
        boolean doPrint = job.printDialog();
        if (doPrint) {
            try {
                job.print();
            } catch (PrinterException e) {
                // The job did not successfully
                // complete
                System.out.println("Some problem happened...");
            }
        }
        System.out.println("printed...");
    }
}
