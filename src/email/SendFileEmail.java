/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

/**
 *
 * @author given
 */
// File Name SendFileEmail.java
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class SendFileEmail {

    public static void main(String[] args) {

        // Recipient's email ID needs to be mentioned.
        List<String> to = new ArrayList<>();
        to.add("email2@gmail.com");
        to.add("email3@gmail.com");
        List<String> cc = new ArrayList<>();
        cc.add("email4@gmail.com");
        List<String> bcc = new ArrayList<>();
        bcc.add("email@gmail.com");

        // Sender's email ID needs to be mentioned
        final String from = "admin@gmail.com";
        final String password = "admin-password";

        // Assuming you are sending email from localhost
        String host = "smtp.gmail.com";

        // Get system properties
        // Setup mail server
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        // -- Attaching to default Session, or we could start a new one --
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header. Compulsary
            if (to != null) {
                if (!to.isEmpty()) {
                    Address[] addressTo = new Address[to.size()];
                    int i = 0;
                    for (String s : to) {
                        addressTo[i++] = new InternetAddress(s);
                    }
                    message.addRecipients(Message.RecipientType.TO, addressTo);
                }
            }

            //Set CC: header filed of the header. Not compulsary
            if (cc != null) {
                if (!cc.isEmpty()) {
                    Address[] addressCC = new Address[cc.size()];
                    int i = 0;
                    for (String s : cc) {
                        addressCC[i++] = new InternetAddress(s);
                    }
                    message.addRecipients(Message.RecipientType.CC, addressCC);
                }
            }

            //Set BCC: header filed of the header. Not compulsary
            if (bcc != null) {
                System.out.println("in BCC");
                if (!bcc.isEmpty()) {
                    Address[] addressBCC = new Address[bcc.size()];
                    int i = 0;
                    for (String s : bcc) {
                        addressBCC[i++] = new InternetAddress(s);
                    }
                    message.addRecipients(Message.RecipientType.BCC, addressBCC);
                }
            }



            // Set Subject: header field
            message.setSubject("Testing file attachment");

            // Create the message part 
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText("This is a test to see if email has sent a file attached. Has the attachment worked?");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            String filename = "file.txt";
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);

            // Send message
            Transport.send(message);
            System.out.println("Sent email successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}
