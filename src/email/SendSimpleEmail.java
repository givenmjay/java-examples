/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

/**
 *
 * @author given
 */
// File Name SendEmail.java
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class SendSimpleEmail {

    public static void main(String[] args) {
        // Recipient's email ID needs to be mentioned.        
        String[] to = {"email1@gmail.com"};
        
        // Sender's email ID needs to be mentioned
        final String from = "admin@gmail.com";
        final String password = "password";

        // Assuming you are sending email from localhost
        String host = "smtp.gmail.com";

        // Setup mail server
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        // -- Attaching to default Session, or we could start a new one --
        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            Address[] addressTo = new Address[to.length];
            for (int i = 0; i < to.length; i++)
            {
                addressTo[i] = new InternetAddress(to[i]);
            }
            message.addRecipients(Message.RecipientType.TO, addressTo);

            // Set Subject: header field
            message.setSubject("This is a test from mshengu!");

            // Now set the actual message
            message.setText("Reply saying you have received the email so I know if it works. Thanks.");
            
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}